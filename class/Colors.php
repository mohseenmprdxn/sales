<?php
class Colors
{
    public function getColors($integer, $array)
    {
        $totalRound = array();
        // representing array numbers as colors
        $colors = $array;
        $integer = count($colors);
        echo '<br>Total number of socks in the pile: '. $integer.'<br><br>';
        echo 'Integers inside array: ';
        foreach ($colors as $value) {
            echo $value.' ';
        }
        $collection = array_count_values($colors);
        foreach($collection as $value) {
            $total = $value/2 ;
            $round = floor($total) . ' ';
            array_push($totalRound,$round);
        }
        $sumOfPairs = array_sum($totalRound);
        echo '<br>Total number of pairs: ' .$sumOfPairs;
    } 
}
?>
